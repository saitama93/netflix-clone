import {computed, inject, Injectable, signal, WritableSignal} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders, HttpParams} from '@angular/common/http';
import {Movie, MovieApiResponse} from './model/movie.model';
import { State } from './model/state.model';
import {environment} from '../../environments/environment';
import { GenreResponse } from './model/genre.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import {MoreInfosComponent} from "../home/more-infos/more-infos.component";

@Injectable({
  providedIn: 'root'
})
export class TmdbService {

  http = inject(HttpClient);
  modalService = inject(NgbModal);

  baseUrl = 'https://api.themoviedb.org';

  private fetchTrendMovie$: WritableSignal<State<MovieApiResponse, HttpErrorResponse>> = signal(State.Builder<MovieApiResponse, HttpErrorResponse>().forInit().build());
  private genres$: WritableSignal<State<GenreResponse, HttpErrorResponse>> = signal(State.Builder<GenreResponse, HttpErrorResponse>().forInit().build());
  private moviesByGenre$: WritableSignal<State<MovieApiResponse, HttpErrorResponse>> = signal(State.Builder<MovieApiResponse, HttpErrorResponse>().forInit().build());
  private movieById$: WritableSignal<State<Movie, HttpErrorResponse>> = signal(State.Builder<Movie, HttpErrorResponse>().forInit().build());
  private search$: WritableSignal<State<MovieApiResponse, HttpErrorResponse>> = signal(State.Builder<MovieApiResponse, HttpErrorResponse>().forInit().build());

  public fetchTrendMovie = computed(() => this.fetchTrendMovie$());
  public genres = computed(() => this.genres$());
  public moviesByGenre = computed(() => this.moviesByGenre$());
  public movieById = computed(() => this.movieById$());
  public search = computed(() => this.search$());

  getTrends(): void
  {
    this.http.get<MovieApiResponse>(`${this.baseUrl}/3/trending/movie/day`, {headers: this.getHeaders()})
      .subscribe({
        next: tmdbResponse => this.fetchTrendMovie$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forSuccess(tmdbResponse).build()),
        error: error => this.fetchTrendMovie$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forError(error).build()),
      });
  }

  getAllGenres(): void
  {
    this.http.get<GenreResponse>(`${this.baseUrl}/3/genre/movie/list`, {headers: this.getHeaders()})
      .subscribe({
        next: (genreResponse) => this.genres$.set(State.Builder<GenreResponse, HttpErrorResponse>().forSuccess(genreResponse).build()),
        error: error => this.genres$.set(State.Builder<GenreResponse, HttpErrorResponse>().forError(error).build()),
      });
  }

  getMoviesByGenre(genreId: number): void
  {
    let queryParam = new HttpParams();
    queryParam = queryParam.set("language", "en-US");
    queryParam = queryParam.set("with_genres", genreId);

    this.http.get<MovieApiResponse>(`${this.baseUrl}/3/discover/movie`, {headers: this.getHeaders(), params: queryParam})
      .subscribe({
        next: moviesByGenre => {
          moviesByGenre.genreId = genreId;
          this.moviesByGenre$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forSuccess(moviesByGenre).build())
        },
        error: error => this.moviesByGenre$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forError(error).build()),
      });
  }

  getMovieById(movieId: number): void
  {
    this.http.get<Movie>(`${this.baseUrl}/3/movie/${movieId}`, {headers: this.getHeaders()})
      .subscribe({
        next: movieResponse => {
          this.movieById$.set(State.Builder<Movie, HttpErrorResponse>().forSuccess(movieResponse).build())
        },
        error: error => this.movieById$.set(State.Builder<Movie, HttpErrorResponse>().forError(error).build()),
      });
  }

  openMoreInfos(movieId: number): void
  {
    let moreInfoModal = this.modalService.open(MoreInfosComponent);
    moreInfoModal.componentInstance.movieId = movieId;
  }

  searchByTerm(term: string): void
  {
    let queryParam = new HttpParams();
    queryParam = queryParam.set("language", "en-US");
    queryParam = queryParam.set("query", term);

    this.http.get<MovieApiResponse>(`${this.baseUrl}/3/search/movie`, {headers: this.getHeaders(), params: queryParam})
      .subscribe({
        next: searchByTerm => {
          this.search$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forSuccess(searchByTerm).build())
        },
        error: error => this.search$.set(State.Builder<MovieApiResponse, HttpErrorResponse>().forError(error).build()),
      });
  }

  getHeaders(): HttpHeaders {
    return new HttpHeaders().set('Authorization', `Bearer ${environment.TMDB_API_KEY}`);
  }

  getImageUrl(id: string, size: 'original' | 'w500' | 'w200'): string
  {
    return `https://image.tmdb.org/t/p/${size}/${id}`;
  }

  clearGetMovieById(): void
  {
    this.movieById$.set(State.Builder<Movie, HttpErrorResponse>().forInit().build())
  }
}
