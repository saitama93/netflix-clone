import { IconDefinition } from '@fortawesome/free-regular-svg-icons';
import { faCircleInfo, faPlay, faSearch } from '@fortawesome/free-solid-svg-icons';

export const fontAwesomeIcons: IconDefinition[] = [
  faPlay,
  faCircleInfo,
  faSearch
]
